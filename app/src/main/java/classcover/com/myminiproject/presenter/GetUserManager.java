package classcover.com.myminiproject.presenter;

import java.util.List;
import java.util.Objects;

/**
 * Created by Henry on 05/12/2016.
 */

public interface GetUserManager {

    interface OnFinishedListener {
        void onFinished(List<Objects> objects);
        void onLog(String message);
    }

    void getUsers(OnFinishedListener listener);
}
