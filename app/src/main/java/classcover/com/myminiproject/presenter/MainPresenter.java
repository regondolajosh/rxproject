package classcover.com.myminiproject.presenter;

/**
 * Created by Henry on 05/12/2016.
 */

public interface MainPresenter {

    void onResume();

    void onItemClicked(int position);

    void onDestroy();


}
