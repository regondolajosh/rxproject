package classcover.com.myminiproject.presenter;

import okhttp3.OkHttpClient;
import retrofit2.GsonConverterFactory;
import retrofit2.Retrofit;
import retrofit2.RxJavaCallAdapterFactory;

/**
 * Created by Henry on 07/12/2016.
 */

public class RestAPIBuilder {

    public static RestAPI buildRetrofitService() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(RestAPI.API_ENDPOINT)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .client(new OkHttpClient.Builder().build())
                .build();

        return retrofit.create(RestAPI.class);
    }
}
