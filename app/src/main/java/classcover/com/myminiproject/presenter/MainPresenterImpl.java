package classcover.com.myminiproject.presenter;

import android.util.Log;

import java.util.List;
import java.util.Objects;

import classcover.com.myminiproject.view.MainView;

/**
 * Created by Henry on 05/12/2016.
 */

public class MainPresenterImpl implements MainPresenter, GetUserManager.OnFinishedListener {

    private MainView mainView;
    private GetUserManager getPostRequest;

    public MainPresenterImpl(MainView mainView, GetUserManager getPostRequest) {

        this.mainView = mainView;
        this. getPostRequest = getPostRequest;
    }

    @Override
    public void onFinished(List<Objects> objects) {
        if(mainView != null) {
            mainView.setItems(objects);
            mainView.hideProgress();
        }

    }

    @Override
    public void onLog(String message) {
        Log.d("JOSH LOG", message );
    }

    @Override
    public void onResume() {
        if(mainView != null) {
            mainView.showProgress();
        }

        getPostRequest.getUsers(this);
    }

    @Override
    public void onItemClicked(int position) {
        if(mainView != null) {
            mainView.showMessage("click");
        }
    }

    @Override
    public void onDestroy() {
        mainView = null;
    }


    public MainView getMainView() {
        return mainView;
    }

    public void setMainView(MainView mainView) {
        this.mainView = mainView;
    }

    public GetUserManager getGetPostRequest() {
        return getPostRequest;
    }

    public void setGetPostRequest(GetUserManager getPostRequest) {
        this.getPostRequest = getPostRequest;
    }



}
