package classcover.com.myminiproject.presenter;

import retrofit2.http.GET;
import retrofit2.http.Path;
import rx.Observable;

/**
 * Created by Henry on 07/12/2016.
 */

public interface RestAPI {

    String API_ENDPOINT = "https://api.github.com/users";

    @GET("/posts/{id}")
    Observable getPost(@Path("id") int id);
}
