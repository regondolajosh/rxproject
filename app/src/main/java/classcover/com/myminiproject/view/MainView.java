package classcover.com.myminiproject.view;

import java.util.List;
import java.util.Objects;

/**
 * Created by Henry on 05/12/2016.
 */

public interface MainView {

    void showProgress();

    void hideProgress();

    void setItems(List<Objects> items);

    void showMessage(String message);
}
